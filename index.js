#!/usr/bin/env node

/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepchecker - See webpages through the eyes of Deepsalter
    Copyright (C) 2016 MySidesTheyAreGone

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

*/

const R = require('ramda')
const pretty = require('prettyoutput')

const _defaultUserAgent = 'Node/' + process.version + ' Deepsalter/v' + require('./package.json').version

let mercuryApiKey = process.argv[2]
let url = process.argv[3]

if (R.isNil(mercuryApiKey) || R.isNil(url)) {
  console.log('Usage: dck <mercury api key> <url>')
  process.exit(1)
}

const W = require('./lib/web.js')({
  webpageUserAgent: _defaultUserAgent,
  maxDownloadSize: 100000000,
  mercuryApiKey: mercuryApiKey
}, (l, m) => console.log('[' + l + '] ' + m))

W.scrape(url)
  .then(R.pipe(
    R.pick(['author', 'title', 'content']),
    R.over(R.lensProp('content'), R.pipe(
      R.defaultTo('WARNING: content empty'),
      R.take(350),
      R.flip(R.concat)(' [...]')
    )),
    pretty,
    console.log
  ))
  .catch(console.error)
