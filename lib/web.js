/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2017 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

*/

module.exports = function (config, log) {
  const _requestUserAgent = config.webpageUserAgent
  const _maxDownloadSize = config.maxDownloadSize // bytes, uncompressed
  const _mercuryApiKey = config.mercuryApiKey

  const _mercuryEndpoint = 'https://mercury.postlight.com/parser'
  const _emptyresponse = {headers: {'content-type': 'text/html'}, body: '<html><head></head><body></body></html>'}
  const _matchers = require('../data/matchers.json')
  const _httpreg = 'https?:\/\/[a-zA-Z0-9]*\.?' // eslint-disable-line

  const R = require('ramda')
  const CF = require('./cfsolver.js')
  const urllib = require('url')
  const cookielib = require('tough-cookie').Cookie
  const cheerio = require('cheerio')
  const got = require('got')

  const guessers = require('../data/guessers.json')

  const clean = R.pipe(
    R.replace(/\r?\n|\r/g, ' '),
    R.trim,
    R.replace(/\s+/g, ' ')
  )

  const isMissing = R.either(R.isNil, R.isEmpty)

  function toMegabytes (bytes) {
    return (bytes / (1024 * 1024)).toFixed(2) + 'MB'
  }

  const makeRegExp = R.curry((options, exp) => {
    return new RegExp(exp, options)
  })

  const interval = (n) => new Promise((resolve) => setTimeout(resolve, n * 1000))

  function updateCookies (headers, options) {
    let cookies
    if (!isMissing(headers['set-cookie'])) {
      if (R.is(Array, headers['set-cookie'])) {
        cookies = R.map(cookielib.parse, headers['set-cookie'])
      } else {
        cookies = R.of(cookielib.parse(headers['set-cookie']))
      }
      let newCookies = R.join('; ', R.map(R.invoker(0, 'cookieString'), cookies))
      if (isMissing(options.headers.cookie)) {
        options.headers.cookie = newCookies
      } else {
        options.headers.cookie += '; ' + newCookies
      }
    }
  }

  function limitedRequest (url, opts) {
    let options = R.clone(opts)
    let limit = Infinity
    if (!R.isNil(options.sizeLimit)) {
      limit = options.sizeLimit
      options = R.dissoc('sizeLimit', options)
    }
    return new Promise((resolve, reject) => {
      let settled = false
      let length = 0
      let request
      let headers
      let parseable = true
      let cloudflare = false
      let body = ''
      let err
      got.stream(url, options)
        .on('request', r => {
          request = r
        })
        .on('response', (resp) => {
          if (settled) return
          let hasNoContentType = R.isNil(resp.headers['content-type'])
          let isNotHTML = resp.headers['content-type'].indexOf('text/html') === -1
          let isNotXHTML = resp.headers['content-type'].indexOf('application/xhtml') === -1
          if (hasNoContentType || (isNotHTML && isNotXHTML)) {
            settled = true
            parseable = false
            request.abort()
            resolve(R.merge({options, cloudflare, parseable}, _emptyresponse))
          }
          headers = resp.headers
        })
        .on('data', (chunk) => {
          if (settled) return
          length += chunk.length
          if (length > limit) {
            settled = true
            request.abort()
            resolve(R.merge({options, cloudflare, parseable}, _emptyresponse))
            return
          }
          body += chunk.toString('utf8')
        })
        .on('redirect', (r, o) => {
          if (settled) return
          headers = r.headers
          o.url = o.href
          updateCookies(headers, o)
          options = R.clone(o)
          cloudflare = false
        })
        .on('error', (e) => {
          if (settled) return
          err = e
          headers = e.headers
          if (!isMissing(e.headers) && e.headers.server === 'cloudflare-nginx') {
            cloudflare = true
          } else {
            reject(e)
          }
        })
        .on('end', () => {
          if (settled) return
          log('silly', toMegabytes(length) + ' downloaded from ' + url)
          settled = true
          if (!isMissing(err) && !cloudflare) {
            reject(err)
            return
          }
          updateCookies(headers, options)
          resolve({body, options, cloudflare, parseable})
        })
    })
  }

  async function rawRequest (url, opts) {
    let response = await got(url, opts)
    return response.body
  }

  async function requestJSON (url) {
    let response = await got(url, {json: true})
    return response.body
  }

  async function requestWebpage (sizeLimit, url, options = false) {
    let opts = options || {
      url: url,
      sizeLimit: sizeLimit,
      retries: 0,
      decompress: true,
      headers: {'User-Agent': _requestUserAgent},
      followRedirect: true
    }
    let response = await limitedRequest(opts.url, opts)
    if (response.cloudflare) {
      let deprotection = CF.deprotect(response.options, response)
      if (deprotection.action === 'done') {
        return {body: response.body, parseable: response.parseable}
      } else if (deprotection.action === 'delayed request') {
        await interval(CF.cloudflareDelay)
        return requestWebpage(sizeLimit, url, deprotection.payload)
      } else if (deprotection.action === 'request') {
        return requestWebpage(sizeLimit, url, deprotection.payload)
      }
    } else {
      return {body: response.body, parseable: response.parseable}
    }
  }

  const unleak = (s) => (' ' + s).substr(1)

  const scrapeHTML = R.curry((selector, attr, body) => {
    if (R.isNil(body)) {
      return null
    }
    let jq
    if (R.isNil(body.html) || !R.is(Function, body.html)) {
      jq = cheerio.load(body)
    } else {
      jq = body
    }
    let out
    if (attr === 'text') {
      out = jq(selector).map(function () {
        return jq(this).text()
      }).get()
    } else {
      out = jq(selector).map(function () {
        return jq(this).attr(attr)
      }).get()
    }
    out = R.map(unleak, out)
    return out
  })

  const compiledMatchers = R.map(R.pipe(
    R.over(R.lensProp('site'), R.concat(_httpreg)),
    R.over(R.lensProp('site'), makeRegExp('i')),
    R.over(R.lensProp('query'), scrapeHTML(R.__, 'text'))
  ))(_matchers)

  const findAuthorPrecisely = R.curry((url, body) => {
    // vice is super-retarded
    if (R.test(/https?:\/\/[a-z\.]*vice\.com/i, url)) { // eslint-disable-line
      let author = (/"full_name" *: *"([^"]+)"/i).exec(body)
      if (!(author == null) && !(author[1] == null)) {
        return clean(author[1])
      } else {
        return ''
      }
    }
    let _matchers = R.filter(R.propSatisfies((v) => R.test(v, url), 'site'), compiledMatchers)
    if (R.length(_matchers) > 0) {
      let authors = R.map(R.invoker(1, 'query')(body), _matchers)
      return R.join(' ', R.reduce(R.concat, [], authors))
    } else {
      return ''
    }
  })

  function guessAuthor (body) {
    let jq = cheerio.load(body)
    let author
    for (let guesser of guessers) {
      author = R.head(scrapeHTML(guesser[0], guesser[1], jq))
      if (!R.isNil(author)) {
        return clean(author)
      }
    }
    return ''
  }

  const unarchiveOriginal = R.pipe(
    scrapeHTML('td:contains(Original) ~ td input', 'value'),
    R.head
  )

  const unarchiveFrom = R.pipe(
    scrapeHTML('td:contains(Saved from) ~ td input[type=text]', 'value'),
    R.head
  )

  const unwayback = R.pipe(
    scrapeHTML('#wmtbURL', 'value'),
    R.head,
    R.defaultTo('')
  )

  const ununvisit = R.pipe(
    scrapeHTML('a.perma', 'text'),
    R.head,
    R.defaultTo('')
  )

  const ungoogle = R.pipe(
    scrapeHTML('base', 'href'),
    (g) => {
      console.log(JSON.stringify(g, null, 2))
      return g
    },
    R.head,
    R.defaultTo('')
  )

  async function mercuryParse (url) {
    const request = R.merge(urllib.parse(_mercuryEndpoint), {
      method: 'GET',
      headers: {
        'x-api-key': _mercuryApiKey,
        'User-Agent': _requestUserAgent
      }
    })
    const options = {
      json: true,
      query: {url: url}
    }
    let parsedData = await rawRequest(request, options)
    let out = R.pick(['title', 'content'], parsedData)
    out.url = url
    return out
  }

  const isBlacklisted = R.anyPass([
    R.equals(''),
    R.test(/^https?:\/\/(www\.)?deepfreeze\.it/i),
    R.test(/^https?:\/\/(www\.)?(youtube.com|youtu.be)/i),
    R.test(/^https?:\/\/(www\.)?twitter.com/i),
    R.test(/imgur/i),
    R.test(/^https?:\/\/(www\.|i\.)?redd\.?it/i),
    R.test(/(\.gif|\.jpg|\.png|\.pdf)/i)
  ])

  let maxRss = 0

  async function scrape (url, origin) {
    let mem = process.memoryUsage().rss
    let response
    if (isBlacklisted(url)) {
      response = R.merge({parseable: false}, _emptyresponse)
    } else {
      response = await requestWebpage(_maxDownloadSize, url)
    }
    let body = response.body
    if (!response.parseable) {
      return {author: '', title: '', content: ''}
    } else if (R.test(/^https?:\/\/archive\.(is|fo)/i, url)) {
      let unarchived = unarchiveOriginal(body)
      if (R.either(R.isNil, R.isEmpty)(unarchived)) {
        unarchived = R.defaultTo('', unarchiveFrom(body))
      }
      if (unarchived === '') {
        throw new Error('Could not extract the original URL from archive.is (' + url + '); this is a bug. Giving up.')
      } else {
        return scrape(unarchived, R.defaultTo(url, origin))
      }
    } else if (R.test(/^https?:\/\/web\.archive\.org/i, url)) {
      const unwaybacked = unwayback(body)
      if (unwaybacked === '') {
        throw new Error('Could not extract the original URL from the Wayback Machine (' + url + '); this is a bug. Giving up.')
      } else {
        return scrape(unwaybacked, R.defaultTo(url, origin))
      }
    } else if (R.test(/^https?:\/\/unvis\.it/i, url)) {
      const ununvisited = ununvisit(body)
      if (ununvisited === '') {
        throw new Error('Could not extract the original URL from unvis.it (' + url + '); this is a bug. Giving up.')
      } else {
        return scrape(ununvisited, R.defaultTo(url, origin))
      }
    } else if (R.test(/^https?:\/\/webcache\.googleusercontent\.com/i, url)) {
      const ungoogled = ungoogle(body)
      if (ungoogled === '') {
        throw new Error('Could not extract the original URL from Google Cache (' + url + '); this is a bug. Giving up.')
      } else {
        return scrape(ungoogled, R.defaultTo(url, origin))
      }
    } else {
      let parsedContent
      try {
        parsedContent = await mercuryParse(url)
      } catch (e) {
        if (e.statusCode === 502) {
          parsedContent = {url: url}
        } else {
          log('error', e.stack)
          throw e
        }
      }
      parsedContent.author = findAuthorPrecisely(url, body)
      if (isMissing(parsedContent.author)) {
        parsedContent.author = guessAuthor(body)
      }
      parsedContent.origin = R.defaultTo(url, origin)
      let rss = process.memoryUsage().rss
      maxRss = R.max(maxRss, rss)
      log('silly', 'Net change in memory after scraping: ' + ((rss - mem) / (1024 * 1024)).toFixed(2) + 'Mb')
      log('silly', 'Highest memory usage so far: ' + (maxRss / (1024 * 1024)).toFixed(2) + 'Mb')
      return parsedContent
    }
  }

  return {rawRequest, requestJSON, requestWebpage, scrape}
}
